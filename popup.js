window.onload = function start() {
    update();
};

function update() {
    timer = $("#timer");
    baseUrl = 'http://91.225.131.53:8080/pomidorka-ws-1.0-SNAPSHOT/api';

    window.setInterval(function () {
        $.get(baseUrl + "/pomidorka/status", function (data, status) {
            if (data.status == "RUNNING") {
                if (data.type == "WORK") {
                    timer.css("color", "red");
                } else if (data.type == "REST") {
                    timer.css("color", "blue");
                }
                ts = new Date(data.stopTs - new Date().getTime());
                timer.text(ts.getMinutes() + ":" + ts.getSeconds());
            } else {
                timer.css("color", "black");
                timer.text("00:00");
            }
        });
    }, 500);
}

